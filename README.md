Switchroot Android Pie has been abandonded in favor of Android Q.

Please see https://gitlab.com/ZachyCatGames/q-tips-guide for compiling Switchroot Android Q.
# Clean Build:
Follow https://wiki.lineageos.org/devices/foster/build upto "Prepare the device-specific code"  

Make sure to init with "lineage-16.0", not "lineage-15.1" or anything else during "Initialize the LineageOS source repository"
```bash
git clone https://gitlab.com/switchroot/android/manifest.git -b lineage-16.0 .repo/local_manifests
repo sync
source build/envsetup.sh
repopick -t nvidia-enhancements-p
repopick -t nvidia-shieldtech-p
repopick -t nvidia-beyonder-p
repopick -t nvidia-nvgpu-p
repopick -t icosa-bt
repopick 272671
cd frameworks/base
patch -p1 < ../../.repo/local_manifests/patches/frameworks_base-rsmouse.patch
cd ../../device/nvidia/foster_tab
patch -p1 < ../../../.repo/local_manifests/patches/device_nvidia_foster_tab-beyonder.patch
cd ../../../
export USE_CCACHE=1
ccache -M 50G
```
Run one of the following:

`lunch lineage_icosa-userdebug`: For No Nvidia Stuff  
`lunch lineage_foster_tab-userdebug`: For Nvidia Stuff  
`lunch lineage_foster-userdebug`: For Android TV with Nvidia Stuff  

Add 
```
BOARD_MKBOOTIMG_ARGS    += --cmdline " "
``` 
at line 53 in `device/nvidia/foster/BoardConfig.mk`
In the same file, comment out or remove
```
BOARD_KERNEL_BASE       := 0x80080000
```
Now run:
```bash
make bacon
```

Download hekate and yeet it's contents on the SD card.

Put https://gitlab.com/ZachyCatGames/shitty-pie-guide/-/raw/master/res/00-android.ini?inline=false in `/bootloader/ini` on the SD card.

Put these in `/switchroot/android/` on the SD card:
https://cdn.discordapp.com/attachments/667093920005619742/702602200991662210/coreboot.rom  
https://gitlab.com/switchroot/bootstack/switch-uboot-scripts/-/jobs/artifacts/master/raw/common.scr?job=build    
https://gitlab.com/switchroot/bootstack/switch-uboot-scripts/-/jobs/artifacts/master/raw/sd.scr?job=build  
and rename sd.scr to boot.scr.

Go to `out/target/product/[device name]` (ex. `out/target/product/icosa`) in your lineage source directory and copy `lineage-16.0-[date]-UNOFFICIAL-[device name].zip` to anywhere on the SD card.

Copy `boot.img` and `obj/KERNEL_OBJ/arch/arm64/boot/dts/tegra210-icosa.dtb` from that same directory to `/switchroot/install` on the SD card.

Download https://gitlab.com/ZachyCatGames/shitty-pie-guide/-/raw/master/res/twrp.img and copy it to `/switchroot/install`.

Launch Hekate and navigate to `Tools` -> `Arch bit • RCM • Touch • Partitions` -> `Partition SD Card`.

You can use the `Android ` slider to choose how much storage you want Android to have access too, then press `Next Step`, then `Start`.

Once that finishes press `Flash Android`, then `Continue`.

Press `Continue` again and it should reboot to TWRP.

In TWRP tap mount, then check any/every partition you can.

Now go back and press `Install` then navigate to `external_sd` then to wherever you put `lineage-16.0-[date]-UNOFFICIAL-[device name].zip` and tap on it.

Swipe to confirm and it should install the rest of Android.

Now just reboot, load hekate again, select the config, and hope it boots


# Updating Sources:
```bash
repo forall -c 'git reset --hard'
repo sync
source build/envsetup.sh
repopick -t nvidia-enhancements-p
repopick -t nvidia-shieldtech-p
repopick -t nvidia-beyonder-p
repopick -t nvidia-nvgpu-p
repopick -t joycon-p 
repopick -t icosa-bt
repopick 272671
repopick -t backuptool-cleanup-p
cd frameworks/base
patch -p1 < ../../.repo/local_manifests/patches/frameworks_base-rsmouse.patch
cd ../../device/nvidia/foster_tab
patch -p1 < ../../../.repo/local_manifests/patches/device_nvidia_foster_tab-beyonder.patch
cd ../../../
export USE_CCACHE=1
ccache -M 50G
```

Run the `lunch` command of your choice.

Readd 
```
BOARD_MKBOOTIMG_ARGS    += --cmdline " "
``` 
at line 53 in `device/nvidia/foster/BoardConfig.mk`
In the same file, comment out or remove
```
BOARD_KERNEL_BASE       := 0x80080000
```
Now run:
```bash
make bacon
```

Throw `lineage-16.0-[date]-UNOFFICIAL-[device name].zip` from `out/target/product/[device name]` somewhere on the fat32 partition on sd card and install it with TWRP.
